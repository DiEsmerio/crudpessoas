FROM java:8
COPY ./target/CrudPessoa-1.0.0.jar .
ENTRYPOINT ["java"]
CMD ["-jar", "CrudPessoa-1.0.0.jar"]