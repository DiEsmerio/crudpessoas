package me.app.dto;

import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import lombok.Data;
import me.app.serialization.DateDeserializer;
import me.app.serialization.DateSerializer;

@Data
public class PessoaDto {
	private Integer id;
	private String nome;
	private String cpf;
	@JsonDeserialize(converter = DateDeserializer.class)
	@JsonSerialize(converter = DateSerializer.class)
	private Date dataNascimento;

	@JsonGetter("idade")
	public Long getIdade() {
		if (dataNascimento == null)
			return null;
		OffsetDateTime then = dataNascimento.toInstant().atOffset(ZoneOffset.UTC);
		OffsetDateTime now = Instant.now().atOffset(ZoneOffset.UTC);
		return then.until(now, ChronoUnit.YEARS);
	}

	@JsonGetter("location")
	public String getLocation() {
		if (getId() == null) {
			return null;
		}
		return "/pessoa/" + id;
	}
}
