package me.app.dto;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.function.Function;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;

@Getter
@JsonInclude(Include.NON_NULL)
public class PageDto<Dto> {

	private List<Dto> results;
	private String nextPage;

	public PageDto(List<Dto> results, String nextPage, Map<String, String> args, Function<Dto, Integer> projection) {
		this.results = results;
		if (nextPage != null && results.size() == 5) {
			Optional<Integer> nextProjection = results.stream().map(projection).max(Comparator.naturalOrder());
			String page = "/" + nextPage + "?nextPage=" + nextProjection.orElse(0);
			for (Entry<String, String> entry : args.entrySet())
				if (!"nextPage".equalsIgnoreCase(entry.getKey()))
					page += "&" + entry.getKey() + "=" + entry.getValue();

			this.nextPage = page;
		}
	}
}
