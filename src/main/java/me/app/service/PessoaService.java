package me.app.service;

import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.tomcat.util.collections.CaseInsensitiveKeyMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import me.app.dto.PageDto;
import me.app.dto.PessoaDto;
import me.app.exception.CPFException;
import me.app.map.PessoaMapper;
import me.app.model.Pessoa;
import me.app.repository.PessoaRepository;
import me.app.util.Utils;

@Service
public class PessoaService {

	@Autowired
	private PessoaRepository repository;

	public Optional<PessoaDto> addPessoa(PessoaDto pessoa) throws CPFException {
		Optional<Pessoa> exists = repository.findByCpf(pessoa.getCpf());
		if (exists.isPresent())
			throw new CPFException(PessoaMapper.map(exists.get()));
		if (validate(pessoa)) {
			Pessoa p = repository.save(PessoaMapper.map(pessoa));
			Optional<Pessoa> ent = repository.findById(p.getId());
			Optional<PessoaDto> dto = ent.map(PessoaMapper::map);
			return dto;
		}
		return Optional.empty();
	}

	@Autowired
	private EntityManager entityManager;

	public PageDto<PessoaDto> getPessoas(Map<String, String> params) {
		CriteriaBuilder qb = entityManager.getCriteriaBuilder();
		CriteriaQuery<Pessoa> q = qb.createQuery(Pessoa.class);
		Root<Pessoa> root = q.from(Pessoa.class);
		CaseInsensitiveKeyMap<String> paramIns = new CaseInsensitiveKeyMap<String>();
		paramIns.putAll(params);
		Predicate p = qb.conjunction();
		if (paramIns.containsKey("cpf"))
			p = qb.equal(root.get("cpf"), Utils.numbersOnly(paramIns.get("cpf")));
		if (paramIns.containsKey("nome"))
			p = qb.and(qb.like(qb.lower(root.get("nome")),
					"%" + String.valueOf(paramIns.get("nome")).replace("*", "%").toLowerCase() + "%"), p);
		if (paramIns.containsKey("nextPage"))
			p = qb.and(p,
					qb.greaterThan(root.get("id"), Integer.valueOf(Utils.numbersOnly("0" + paramIns.get("nextPage")))));
		q = q.select(root).where(p);

		return new PageDto<>(entityManager.createQuery(q).setMaxResults(5).getResultStream().map(PessoaMapper::map)
				.collect(Collectors.toList()), "pessoas", params, e -> e.getId());

	}

	public Optional<PessoaDto> getPessoa(Integer id) {
		Optional<Pessoa> p = repository.findById(id);
		return p.map(PessoaMapper::map);
	}

	private boolean validateCPF(String cpf) {
		if (cpf == null)
			return false;
		int[] digits = cpf.chars().filter(Character::isDigit).map(e -> e - '0').toArray();
		if (digits.length != 11)
			return false;
		int sum = IntStream.range(0, 9).map(e -> (10 - e) * digits[e]).sum();
		int remainder = (sum * 10) % 11 % 10;
		if (remainder != digits[9])
			return false;
		sum = IntStream.range(0, 10).map(e -> (11 - e) * digits[e]).sum();
		remainder = (sum * 10) % 11 % 10;

		return remainder == digits[10];
	}

	private boolean validate(PessoaDto dto) throws CPFException {
		if (!validateCPF(dto.getCpf()))
			throw new CPFException(dto.getCpf());
		return dto.getDataNascimento() != null && dto.getNome() != null;
	}

	public Optional<PessoaDto> putPessoa(Integer id, PessoaDto dto) throws CPFException {
		Optional<Pessoa> pessoa = repository.findById(id);
		Optional<Pessoa> pessoaPorCpf = repository.findByCpf(dto.getCpf());
		if(pessoaPorCpf.isPresent() && pessoaPorCpf.get().getId() != id)
			throw new CPFException(PessoaMapper.map(pessoaPorCpf.get()));
		if (validate(dto))
			return pessoa.flatMap(p -> {
				if ((dto.getId() == null || dto.getId() == id)) {
					Pessoa pn = PessoaMapper.map(dto);
					pn.setId(id);
					pn = repository.save(pn);
					return Optional.of(PessoaMapper.map(pn));
				}
				return Optional.empty();
			});
		return Optional.empty();
		
	}

	public Optional<PessoaDto> patchPessoa(Integer id, PessoaDto dto) throws CPFException {
		Optional<Pessoa> pessoa = repository.findById(id);
		if (dto.getCpf() != null)
			if (!validateCPF(dto.getCpf()))
				throw new CPFException(dto.getCpf());
		return pessoa.flatMap(p -> {
			if (dto.getCpf() != null)
				p.setCpf(dto.getCpf());
			if (dto.getNome() != null)
				p.setNome(dto.getNome());
			if (dto.getDataNascimento() != null)
				p.setDataNascimento(dto.getDataNascimento());
			p = repository.save(p);
			return Optional.of(PessoaMapper.map(p));

		});
	}

	public boolean deletePessoa(Integer id) {
		if (repository.existsById(id)) {
			repository.deleteById(id);
			return true;
		}
		return false;
	}

}
