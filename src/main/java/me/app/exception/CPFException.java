package me.app.exception;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Getter;
import me.app.dto.PessoaDto;

public class CPFException extends Exception {

	@AllArgsConstructor
	@Getter
	@JsonInclude(Include.NON_NULL)
	public static class Dto {
		private String message;
		private PessoaDto pessoa;
	}

	private static final long serialVersionUID = 1L;
	private Dto cause;

	public CPFException(PessoaDto pessoaDto) {
		cause = new Dto("CPF " + pessoaDto.getCpf() + " já inserido.", pessoaDto);
	}

	public CPFException(String cpf) {
		String message = cpf == null ? "é obrigatório." : cpf + " não é válido.";
		cause = new Dto("CPF " + message, null);

	}

	public Dto getDto() {
		return cause;
	}

}
