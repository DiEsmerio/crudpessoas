package me.app.serialization;

import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import com.fasterxml.jackson.databind.util.StdConverter;

public class DateSerializer extends StdConverter<Date, String> {

	private static DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
	
	@Override
	public String convert(Date value) {
		
		return df.format(value);
	}

}
