package me.app.serialization;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.fasterxml.jackson.databind.util.StdConverter;

public class DateDeserializer extends StdConverter<String, Date> {

	private static DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
	
	@Override
	public Date convert(String value) {
		try {
			return df.parse(value);
		} catch(ParseException pe) {
			throw new RuntimeException("Data inválida", pe);	
		}
		}

}
