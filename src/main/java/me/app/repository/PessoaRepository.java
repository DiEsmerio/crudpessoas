package me.app.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import me.app.model.Pessoa;

public interface PessoaRepository extends CrudRepository<Pessoa, Integer> {
	
	public Optional<Pessoa> findByCpf(String cpf);
}
