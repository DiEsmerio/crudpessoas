package me.app.util;

public class Utils {

	public static String numbersOnly(String s) {
		StringBuilder sb = new StringBuilder();
		if (s != null)
			;
		s.chars().filter(Character::isDigit).forEachOrdered(c -> sb.append((char) c));
		return sb.toString();
	}

	private static String CPF_PATTERN = "xxx.xxx.xxx-xx";

	public static String format(String pattern, String s) {
		if (s == null || pattern == null)
			return "";
		int offset = 0;
		char[] input = s.toCharArray();
		char[] output = pattern.toCharArray();
		for (int k = 0; k < input.length; ++k) {
			while (offset < output.length && output[offset] != 'x')
				++offset;
			if (offset == output.length)
				break;
			output[offset++] = input[k];
		}
		return new String(output);
	}

	public static String formatCPF(String s) {
		return format(CPF_PATTERN, s);
	}
}
