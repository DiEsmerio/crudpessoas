package me.app.controller;

import java.net.URI;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import me.app.dto.PageDto;
import me.app.dto.PessoaDto;
import me.app.exception.CPFException;
import me.app.service.PessoaService;

@RestController
public class PessoaController {

	@Autowired
	private PessoaService service;

	@PostMapping("/pessoas")
	public ResponseEntity<PessoaDto> addPessoa(@Valid @RequestBody PessoaDto dto) throws CPFException {
		Optional<PessoaDto> pessoa = service.addPessoa(dto);
		return pessoa.map(p -> ResponseEntity.created(URI.create("/pessoa/" + p.getId())).body(p))
				.orElse(ResponseEntity.status(HttpStatus.BAD_REQUEST).build());
	}

	@GetMapping("/pessoas")
	public ResponseEntity<PageDto<PessoaDto>> getPessoas(@RequestParam Map<String, String> params) {
		return ResponseEntity.ok(service.getPessoas(params));
	}

	@GetMapping(path = "/pessoa/{id}")
	public ResponseEntity<PessoaDto> getPessoas(@PathVariable("id") Integer id) {
		Optional<PessoaDto> p = service.getPessoa(id);
		return ResponseEntity.of(p);
	}

	@PutMapping(path = "/pessoa/{id}")
	public ResponseEntity<PessoaDto> putPessoa(@PathVariable("id") Integer id, @Valid @RequestBody PessoaDto dto) throws CPFException {
		Optional<PessoaDto> pessoa = service.putPessoa(id, dto);
		return pessoa.map(ResponseEntity::ok).orElse(ResponseEntity.status(HttpStatus.BAD_REQUEST).build());
	}

	@PatchMapping(path = "/pessoa/{id}")
	public ResponseEntity<PessoaDto> patchPessoa(@PathVariable("id") Integer id, @Valid @RequestBody PessoaDto dto) throws CPFException {
		Optional<PessoaDto> pessoa = service.patchPessoa(id, dto);
		return pessoa.map(ResponseEntity::ok).orElse(ResponseEntity.status(HttpStatus.BAD_REQUEST).build());
	}

	@DeleteMapping(path = "/pessoa/{id}")
	public ResponseEntity<PessoaDto> deletePessoas(@PathVariable("id") Integer id) {
		return ResponseEntity.status(service.deletePessoa(id) ? HttpStatus.OK : HttpStatus.NOT_FOUND).build();
	}
	
	@ExceptionHandler(CPFException.class)
	public ResponseEntity<CPFException.Dto> handleCPFException(CPFException exc){
		return ResponseEntity.status(HttpStatus.CONFLICT).body(exc.getDto());
	}
}
