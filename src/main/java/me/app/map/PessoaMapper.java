package me.app.map;

import me.app.dto.PessoaDto;
import me.app.model.Pessoa;
import me.app.util.Utils;

public class PessoaMapper {

	public static PessoaDto map(Pessoa ent) {
		PessoaDto dto = new PessoaDto();
		dto.setCpf(Utils.formatCPF(ent.getCpf()));
		dto.setDataNascimento(ent.getDataNascimento());
		dto.setId(ent.getId());
		dto.setNome(ent.getNome());
		return dto;
	}

	public static Pessoa map(PessoaDto dto) {
		Pessoa ent = new Pessoa();
		ent.setCpf(Utils.numbersOnly(dto.getCpf()));
		ent.setDataNascimento(dto.getDataNascimento());
		ent.setNome(dto.getNome());
		return ent;

	}

}
