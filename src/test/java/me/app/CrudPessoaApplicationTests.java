package me.app;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes=CrudPessoaApplicationTests.class)
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
public class CrudPessoaApplicationTests {

	@Test
	public void contextLoads() {
	}
	
}
